import './App.css';

import { createChart } from 'lightweight-charts';


function App() {

  const chart = createChart(document.body, { width: 400, height: 300 });
  const lineSeries = chart.addLineSeries();
  lineSeries.setData([
      { time: '2019-04-11', value: 80.01 },
      { time: '2019-04-12', value: 96.63 },
      { time: '2019-04-13', value: 76.64 },
      { time: '2019-04-14', value: 81.89 },
      { time: '2019-04-15', value: 74.43 },
      { time: '2019-04-16', value: 80.01 },
      { time: '2019-04-17', value: 96.63 },
      { time: '2019-04-18', value: 76.64 },
      { time: '2019-04-19', value: 81.89 },
      { time: '2019-04-20', value: 74.43 },
  ]);

  return (
    <div className="App">
        <label htmlFor="pet-select">Выбор инструмента:</label>

        <select name="tools" id="tools">
            <option value="ticker_00">ticker_00</option>
            <option value="ticker_01">ticker_01</option>
            <option value="ticker_02">ticker_02</option>
            <option value="ticker_03">ticker_03</option>
            <option value="ticker_04">ticker_04</option>
        </select>
    </div>
  );
}

export default App;
