import orjson
from websockets.legacy.client import connect


class WebSocketClient:
    def __init__(self, host: str):
        self.host = host

    async def connect(self) -> None:
        self.connection = await connect(
            self.host, ping_interval=None, ping_timeout=None, max_queue=10000, open_timeout=None
        )

    async def send(self, data: dict) -> None:
        await self.connection.send(orjson.dumps(data).decode())

    async def receive(self) -> None:
        pass

    async def disconnect(self) -> None:
        await self.connection.close()
