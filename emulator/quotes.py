import random


def generate_quote() -> int:
    """Return the random quote value -1 or 1"""
    movement = random.randint(0, 10) / 10
    return -1 if movement < 0.5 else 1
